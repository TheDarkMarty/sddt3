function init() {
    M.AutoInit();
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, options);
      });
}

function coinFlip(/*coins*/) {
    const ncoins = document.getElementById("ncoins").value;
    let heads = 0
    let tails = 0
    if (ncoins == 1) {
        flip = Math.round(Math.random());
        //console.log(flip) /* was used for testing what was rolled with what was output*/
        if(flip == 1) {
            document.getElementById("result").innerHTML = "You got Heads!";
            /*if (document.getElementsByClassName("flipresult").length == 0) {
                let result = document.createElement('p');
                result.textContent = "You got Heads";
                result.setAttribute('class', 'flipresult');
                document.getElementById("results").appendChild(result); 
              } else {
                document.getElementsByClassName("flipresult")[0].innerHTML = "You got Heads"
              } */
        } else {
            document.getElementById("result").innerHTML = "You got Tails!";
            /*if (document.getElementsByClassName("flipresult").length == 0) {
                let result = document.createElement('p');
                result.textContent = "You got Tails";
                result.setAttribute('class', 'flipresult');
                document.getElementById("results").appendChild(result); 
              } else {
                document.getElementsByClassName("flipresult")[0].innerHTML = "You got Tails"
              } */
        }
    } else {
        for (let i = 0; i < ncoins; i++) {
            flip = Math.round(Math.random());
            //console.log(flip) /* was used for testing what was rolled with what was output*/
            if(flip == 1) {
                heads += 1
            } else {
                tails += 1
            }
        }
        document.getElementById("result").innerHTML = "You got " + heads + " Heads<br>You got " + tails + " Tails";
        /*if (document.getElementsByClassName("flipresult").length == 0) {
            let result = document.createElement('p');
            result.innerHTML = "You got " + heads + " Heads<br>You got " + tails + " Tails";
            result.setAttribute('class', 'flipresult');
            document.getElementById("results").appendChild(result); 
            } else {
            document.getElementsByClassName("flipresult")[0].innerHTML = "You got " + heads + " Heads<br>You got " + tails + " Tails";
        } */
    }
}
dice = {
    d20: "20",
    d12: "12",
    d10: "10",
    d8: "8",
    d6: "6",
    d4: "4"
}
//create switch case statement between all the different dice then link to a function of other switch case statements which get the result of the roll of that die
function diceRoll(/*diceType, ndice*/) {
    if(document.getElementById("20").checked) {
        diceType = "20"
    } else if(document.getElementById("12").checked) {
        diceType = "12"
    } else if(document.getElementById("10").checked) {
        diceType = "10"
    } else if(document.getElementById("8").checked) {
        diceType = "8"
    } else if(document.getElementById("6").checked) {
        diceType = "6"
    } else if(document.getElementById("4").checked) {
        diceType = "4"
    }
    const ndice = document.getElementById("ndice").value;
    switch (diceType) {
        case "20":
            rollTheDice(diceType, ndice);
            break;
        case "12":
            rollTheDice(diceType, ndice);
            break;
        case "10":
            rollTheDice(diceType, ndice);
            break;
        case "8":
            rollTheDice(diceType, ndice);
            break;
        case "6":
            rollTheDice(diceType, ndice);
            break;
        case "4":
            rollTheDice(diceType, ndice);
            break;
    }
}

function rollTheDice(diceType, ndice) {
    let one = 0, two = 0, three = 0, four = 0, five = 0, six = 0, seven = 0, eight = 0, nine = 0, ten = 0, eleven = 0, twelve = 0, thirteen = 0, fourteen = 0, fifteen = 0, sixteen = 0, seventeen = 0, eighteen = 0, nineteen = 0, twenty = 0;
    document.getElementById("result").innerHTML = "";
    for (let i = 0; i < ndice; i++) {
        roll = Math.ceil(Math.random() * parseInt(diceType));
        console.log(roll)
        switch (roll) {
            case 1:
                one++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 1!";
                } else {
                    if(document.getElementById("resone") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + one + " one!";
                        result.id = 'resone';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("resone").innerHTML = "You got " + one + " one!";
                    }
                }
                break;
            case 2:
                two++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 2!";
                } else {
                    
                    if(document.getElementById("restwo") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + two + " two!";
                        result.id = 'restwo';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("restwo").innerHTML = "You got " + two + " two!";
                    }
                }
                break;
            case 3:
                three++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 3!";
                } else {
                    
                    if(document.getElementById("resthree") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + three + " three!";
                        result.id = 'resthree';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("resthree").innerHTML = "You got " + three + " three!";
                    }
                }
                break;
            case 4:
                four++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 4!";
                } else {
                    
                    if(document.getElementById("resfour") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + four + " four!";
                        result.id = 'resfour';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("resfour").innerHTML = "You got " + four + " four!";
                    }
                }
                break;
            case 5:
                five++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 5!";
                } else {
                    
                    if(document.getElementById("resfive") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + five + " five!";
                        result.id = 'resfive';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("resfive").innerHTML = "You got " + five + " five!";
                    }
                } 
                break;
            case 6:
                six++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 6!";
                } else {
                    
                    if(document.getElementById("ressix") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + six + " six!";
                        result.id = 'ressix';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("ressix").innerHTML = "You got " + six + " six!";
                    }
                }
                break;
            case 7:
                seven++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 7!";
                } else {
                    
                    if(document.getElementById("resseven") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + seven + " seven!";
                        result.id = 'resseven';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("resseven").innerHTML = "You got " + seven + " seven!";
                    }
                }
                break;
            case 8:
                eight++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 8!";
                } else {
                    
                    if(document.getElementById("reseight") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + eight + " eight!";
                        result.id = 'reseight';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("reseight").innerHTML = "You got " + eight + " eight!";
                    }
                }
                break;
            case 9:
                nine++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 9!";
                } else {
                    
                    if(document.getElementById("resnine") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + nine + " nine!";
                        result.id = 'resnine';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("resnine").innerHTML = "You got " + nine + " nine!";
                    }
                }
                break;
            case 10:
                ten++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 10!";
                } else {
                    
                    if(document.getElementById("resten") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + ten + " ten!";
                        result.id = 'resten';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("resten").innerHTML = "You got " + ten + " ten!";
                    }
                }
                break;
            case 11:
                eleven++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 11!";
                } else {
                    
                    if(document.getElementById("reseleven") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + eleven + " eleven!";
                        result.id = 'reseleven';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("reseleven").innerHTML = "You got " + eleven + " eleven!";
                    }
                }
                break;
            case 12:
                twelve++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 12!";
                } else {
                    
                    if(document.getElementById("restwelve") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + twelve + " twelve!";
                        result.id = 'restwelve';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("restwelve").innerHTML = "You got " + twelve + " twelve!";
                    }
                }
                break;
            case 13:
                thirteen++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 13!";
                } else {
                    
                    if(document.getElementById("resthirteen") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + thirteen + " thirteen!";
                        result.id = 'resthirteen';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("resthirteen").innerHTML = "You got " + thirteen + " thirteen!";
                    }
                }
                break;
            case 14:
                fourteen++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 14!";
                } else {
                    
                    if(document.getElementById("resfourteen") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + fourteen + " fourteen!";
                        result.id = 'resfourteen';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("resfourteen").innerHTML = "You got " + fourteen + " fourteen!";
                    }
                }
                break;
            case 15:
                fifteen++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 15!";
                } else {
                    
                    if(document.getElementById("resfifteen") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + fifteen + " fifteen!";
                        result.id = 'resfifteen';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("resfifteen").innerHTML = "You got " + fifteen + " fifteen!";
                    }
                }
                break;
            case 16:
                sixteen++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 16!";
                } else {
                    
                    if(document.getElementById("ressixteen") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + sixteen + " sixteen!";
                        result.id = 'ressixteen';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("ressixteen").innerHTML = "You got " + sixteen + " sixteen!";
                    }
                }
                break;
            case 17:
                seventeen++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 17!";
                } else {
                    
                    if(document.getElementById("resseventeen") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + seventeen + " seventeen!";
                        result.id = 'resseventeen';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("resseventeen").innerHTML = "You got " + seventeen + " seventeen!";
                    }
                }
                break;
            case 18:
                eighteen++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 18!";
                } else {
                    
                    if(document.getElementById("reseighteen") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + eighteen + " eighteen!";
                        result.id = 'reseighteen';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("reseighteen").innerHTML = "You got " + eighteen + " eighteen!";
                    }
                }
                break;
            case 19:
                nineteen++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 19!";
                } else {
                    
                    if(document.getElementById("resnineteen") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + nineteen + " nineteen!";
                        result.id = 'resnineteen';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("resnineteen").innerHTML = "You got " + nineteen + " nineteen!";
                    }
                }
                break;
            case 20:
                twenty++;
                if(ndice == "1") {
                    document.getElementById("result").innerHTML = "You got a 20!";
                } else {
                    
                    if(document.getElementById("restwenty") == null) {
                        let result = document.createElement('p');
                        result.textContent = "You got " + twenty + " twenty!";
                        result.id = 'restwenty';
                        document.getElementById("result").appendChild(result);
                    } else {
                        document.getElementById("restwenty").innerHTML = "You got " + twenty + " twenty!";
                    }
                }
                break;
            }
    }
}

const cardsindeck = [
    "Ace of Hearts",
    "King of Hearts",
    "Queen of Hearts",
    "Jack of Hearts",
    "Ten of Hearts",
    "Nine of Hearts",
    "Eight of Hearts",
    "Seven of Hearts",
    "Six of Hearts",
    "Five of Hearts",
    "Four of Hearts",
    "Three of Hearts",
    "Two of Hearts",
    "Ace of Diamonds",
    "King of Diamonds",
    "Queen of Diamonds",
    "Jack of Diamonds",
    "Ten of Diamonds",
    "Nine of Diamonds",
    "Eight of Diamonds",
    "Seven of Diamonds",
    "Six of Diamonds",
    "Five of Diamonds",
    "Four of Diamonds",
    "Three of Diamonds",
    "Two of Diamonds",
    "Ace of Clubs",
    "King of Clubs",
    "Queen of Clubs",
    "Jack of Clubs",
    "Ten of Clubs",
    "Nine of Clubs",
    "Eight of Clubs",
    "Seven of Clubs",
    "Six of Clubs",
    "Five of Clubs",
    "Four of Clubs",
    "Three of Clubs",
    "Two of Clubs",
    "Ace of Spades",
    "King of Spades",
    "Queen of Spades",
    "Jack of Spades",
    "Ten of Spades",
    "Nine of Spades",
    "Eight of Spades",
    "Seven of Spades",
    "Six of Spades",
    "Five of Spades",
    "Four of Spades",
    "Three of Spades",
    "Two of Spades"
]

// if they opt to have joker, just have a pre-emptive if statement at the start of the function to check if joker and just append it as needed


function drawCard(/*drawType, ncards*/) {
    const ncards = document.getElementById("ncards").value;
    if(!document.getElementById("drawtype").checked) {
        if(ncards == 1) {
            draw = cardsindeck[Math.round((Math.random() * 52))]
            if(draw[0] == "A" || draw[0] == "E") {
                document.getElementById("result").innerHTML = "You got an " + draw + "!";
            } else {
                document.getElementById("result").innerHTML = "You got a " + draw + "!";
            }
        } else {
            document.getElementById("result").innerHTML = "";
            let drawn = []
            let curreslen = document.getElementsByClassName("cardresult").length; //gets the number of how many results are currently on the screen
            for (let i = 0; i < curreslen; i++) {
                document.getElementsByClassName("cardresult")[0].remove() //removes the previous results so they can be output
            }
            for (let i = 0; i < ncards; i++) {
                draw = cardsindeck[Math.round((Math.random() * 51))]
                //console.log(draw) was used to check if the "no replace" part of the function worked. If it logged a card that had already been drawn with no redraw then there was a bug.
                while (drawn.includes(draw)) {
                    draw = cardsindeck[Math.round((Math.random() * 51))]
                    //console.log(draw) was used to check if the "no replace" part of the function worked. If this while loop did not redraw until a new card was drawn then there was a bug.
                }
                let result = document.createElement('p');
                if(draw[0] == "A" || draw[0] == "E") { // Grammar rules mean that you use "a" when preceding a constant and "an" when preceding a vowel as the first letters.
                    result.textContent = "You got an " + draw + "!";
                } else {
                    result.textContent = "You got a " + draw + "!";
                }
                result.setAttribute('class', 'cardresult');
                document.getElementById("result").appendChild(result);
                drawn.push(draw)
            }
        }
    }
    if(document.getElementById("drawtype").checked) {
        document.getElementById("result").innerHTML = "";
        let curreslen = document.getElementsByClassName("cardresult").length; //gets the number of how many results are currently on the screen
        for (let i = 0; i < curreslen; i++) {
            document.getElementsByClassName("cardresult")[0].remove() //removes the previous results so they can be output
        }
        for (let i = 0; i < ncards; i++) {
            draw = cardsindeck[Math.round((Math.random() * 51))]
            let result = document.createElement('p');
            if(draw[0] == "A" || draw[0] == "E") { // Grammar rules mean that you use "a" when preceding a constant and "an" when preceding a vowel as the first letters.
                result.textContent = "You got an " + draw + "!";
            } else {
                result.textContent = "You got a " + draw + "!";
            }
            result.setAttribute('class', 'cardresult');
            document.getElementById("result").appendChild(result);
        }
    }
}

function randomnumber(/*nofnumbers, range1, range2*/) {
    let curreslen = document.getElementsByClassName("numberresult").length;
    const range1 = document.getElementById("range1").value;
    const range2 = document.getElementById("range2").value;
    for(i = 0; i < curreslen; i++) {
        document.getElementsByClassName("numberresult")[0].remove()
    }
    const nofnumbers = document.getElementById("nofnumbers").value;
    for (i = 0; i < nofnumbers; i++) {
        do {
            number = Math.round(Math.random() * range2)
        } while (number < range1);
        let result = document.createElement('p');
        result.textContent = "You got " + number;
        result.setAttribute('class', 'numberresult');
        document.getElementById("results").appendChild(result);
    }
}
